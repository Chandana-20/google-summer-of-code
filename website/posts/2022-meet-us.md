<!--
.. title: Meet us
.. slug: 2022-meet-us
.. author: Christian Frisson
.. date: 2022-03-25 10:10:11 UTC-04:00
.. tags: 
.. type: text
-->
# Meet us

Come meet us to discuss about our project ideas and your proposals!

## Virtual Open Office Hours

We will host GSoC specials during our weekly [Virtual Open Office Hours](https://sat.qc.ca/en/nouvelles/bureau-ouvert-virtuel-de-la-sat) at the SAT.

### When?

- Friday March 25 2022 13h00-14h00 EDT
- Friday April 8 2022 13h00-14h00 EDT

### Where?

Join us on [Satellite](https://satellite.sat.qc.ca/t2G8jdP/sat-2) (or as fallback on [Google Meet](https://meet.google.com/jbr-pane-ors)).
